import React, { useState } from "react";
import { useSelector } from "react-redux";
import { useLocation } from "react-router-dom";

function DisplayInfo() {
  const numbersOfItemPerPage = 2;
  const location = useLocation();
  const usersposts = useSelector((state) => state.posts);
  const [pagination, setPagination] = useState([]);
  const [start, setStart] = useState(0);
  const [end, setEnd] = useState(numbersOfItemPerPage - 1);
  const onlypost = usersposts.filter(
    (i) => i.userId === location.state.array.id
  );

  const handlepagination = (e) => {
    const pageNubmer = e.target.innerText;
    if (isNaN(pageNubmer)) {
      console.log(pageNubmer);
      if (pageNubmer === "Previous") {
        setStart((prestart) => prestart - numbersOfItemPerPage);
        setEnd((preend) => preend - numbersOfItemPerPage);
        return;
      } else {
        setStart((prestart) => prestart + numbersOfItemPerPage);
        setEnd((preend) => preend + numbersOfItemPerPage);
        return;
      }
    }
    console.log(
      "start index",
      pageNubmer * numbersOfItemPerPage - numbersOfItemPerPage
    );
    console.log("end index", pageNubmer * numbersOfItemPerPage - 1);
    setStart(pageNubmer * numbersOfItemPerPage - numbersOfItemPerPage);
    setEnd(pageNubmer * numbersOfItemPerPage - 1);
  };

  React.useEffect(() => {
    setPagination(onlypost.slice(start, end + 1));
  }, [start, end]);

  return (
    <div className="container">
      <div className="row">
        <div className="col-md-12">
          <h2 style={{ textAlign: "center", textDecoration: "underline" }}>
            Details of user {location.state.array.name}
          </h2>
        </div>
      </div>
      <div className="row">
        <div className="col-md-6 col-md-offset-3">
          <div
            style={{
              border: "1px solid black",
              borderRadius: "10px",
              textAlign: "center",
            }}
          >
            <p style={{ fontWeight: "bold", textDecoration: "underline" }}>
              Name : {location.state.array.name}
            </p>
            <p>Username : {location.state.array.username}</p>
            <p>Email : {location.state.array.email.toLowerCase()}</p>
            <p>Phone : {location.state.array.phone}</p>
            <p>Website : {location.state.array.website}</p>
            <p>Name of Company : {location.state.array.company.name}</p>
            <br></br>
          </div>
        </div>
      </div>
      <div className="row">
        <div className="col-md-12">
          <h2 style={{ textAlign: "center", textDecoration: "underline" }}>
            All Posts of {location.state.array.name}
          </h2>
        </div>
      </div>
      <div className="row">
        {pagination.map((i) => (
          <div className="col-md-6" key={i.id}>
            <div className="postbox">
              <p style={{ fontWeight: "bold" }}>{i.title}</p>
              <p className="bodytext">{i.body}</p>
              <br></br>
            </div>
          </div>
        ))}
      </div>
      <div>
        <nav
          aria-label="Page navigation example"
          style={{ textAlign: "center" }}
        >
          <ul className="pagination justify-content-center">
            <li className={start === 0 ? "page-item disabled" : "page-item"}>
              <a
                className="page-link"
                tabIndex="-1"
                // onClick={(e) => handlepagination(e)}
                onClick={start !== 0 ? (e) => handlepagination(e) : null}
              >
                Previous
              </a>
            </li>
            {[...Array(Math.ceil(onlypost.length / numbersOfItemPerPage))].map(
              (item, index) => (
                <li
                  key={index + 1}
                  className={
                    start === index * numbersOfItemPerPage
                      ? "page-item active"
                      : "page-item"
                  }
                >
                  <a className="page-link" onClick={(e) => handlepagination(e)}>
                    {index + 1}
                  </a>
                </li>
              )
            )}
            <li
              className={
                end ===
                Math.ceil(onlypost.length / numbersOfItemPerPage) *
                  numbersOfItemPerPage -
                  1
                  ? "page-item disabled"
                  : "page-item"
              }
            >
              <a
                className="page-link"
                onClick={
                  end !==
                  Math.ceil(onlypost.length / numbersOfItemPerPage) *
                    numbersOfItemPerPage -
                    1
                    ? (e) => handlepagination(e)
                    : null
                }
              >
                Next
              </a>
            </li>
          </ul>
        </nav>
      </div>
    </div>
  );
}
export default DisplayInfo;
