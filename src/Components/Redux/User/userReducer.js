import {
  FETCH_USERS_POSTS,
  FETCH_USERS_DATA,
  FETCH_USERS_ALBUMS,
} from "./userType";

//initial state
const initialStateuser = {
  users: [],
  posts: [],
  albums: [],
};

const userreducer = (state = initialStateuser, action) => {
  switch (action.type) {
    case FETCH_USERS_DATA:
      return {
        ...state,
        users: action.payload,
      };
    case FETCH_USERS_POSTS:
      return {
        ...state,
        posts: action.payload,
      };
    case FETCH_USERS_ALBUMS:
      return {
        ...state,
        albums: action.payload,
      };
    default:
      return state;
  }
};

export default userreducer;
