import axios from "axios";

import {
  FETCH_USERS_POSTS,
  FETCH_USERS_DATA,
  FETCH_USERS_ALBUMS,
} from "./userType";

const fetchusersrdata = (users) => {
  return {
    type: FETCH_USERS_DATA,
    payload: users,
  };
};
const fetchusersposts = (posts) => {
  return {
    type: FETCH_USERS_POSTS,
    payload: posts,
  };
};
const fetchusersalbums = (albums) => {
  return {
    type: FETCH_USERS_ALBUMS,
    payload: albums,
  };
};

//this is async action creater function
export const fetchusers = () => {
  return async function (dispatch) {
    await axios
      // .get("https://jsonplaceholder.typicode.com/users")
      .get("http://localhost:5000/users/getdata")
      .then((response) => {
        dispatch(fetchusersrdata(response.data));
      })
      .catch((error) => {
        console.log(error.response.data.message);
      });
  };
};

//this is async action creater function
export const fetchsposts = () => {
  return function (dispatch) {
    axios
      .get("https://jsonplaceholder.typicode.com/posts")
      .then((response) => {
        dispatch(fetchusersposts(response.data));
      })
      .catch((error) => {
        console.log(error);
      });
  };
};
//this is async action creater function
export const fetchalbums = () => {
  return function (dispatch) {
    axios
      .get("https://jsonplaceholder.typicode.com/albums")
      .then((response) => {
        dispatch(fetchusersalbums(response.data));
      })
      .catch((error) => {
        console.log(error);
      });
  };
};
