import { createStore, applyMiddleware } from "redux";

import thunk from "redux-thunk";

import userreducer from "./User/userReducer";

const store = createStore(userreducer, applyMiddleware(thunk));

export default store;
