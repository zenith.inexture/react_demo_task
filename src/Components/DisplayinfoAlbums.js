import React from "react";
import { useLocation } from "react-router-dom";

function DisplayInfo() {
  const location = useLocation();
  return (
    <>
      <div className="container">
        <div className="row">
          <div className="col-md-12">
            <h2 style={{ textAlign: "center", textDecoration: "underline" }}>
              Albums of {location.state.name}
            </h2>
          </div>
        </div>
        <div className="row">
          {location.state.array.map((i, index) => (
            <div
              className="col-md-6"
              key={index}
              style={{ textAlign: "center" }}
            >
              <div className="boxalbums">
                <p>
                  {index + 1} :{" "}
                  <span style={{ fontWeight: "bold" }}>{i.title}</span>
                </p>
                <br></br>
              </div>
            </div>
          ))}
        </div>
      </div>
    </>
  );
}
export default DisplayInfo;
