import React from "react";

function Modal(props) {
  const displaydata = props.children;
  return (
    <div
      className="modal fade"
      id="lgmodal"
      tabIndex="-1"
      role="dialog"
      aria-labelledby="gridSystemModalLabel"
    >
      <div className="modal-dialog" role="document">
        <div className="modal-content">
          <div className="modal-header">
            <button
              type="button"
              className="close"
              data-dismiss="modal"
              aria-label="Close"
            >
              <span aria-hidden="true">&times;</span>
            </button>
            <h4 className="modal-title" id="gridSystemModalLabel">
              Personal Details
            </h4>
          </div>
          <div className="modal-body">
            <p>Name : {displaydata.name}</p>
            <p>Username : {displaydata.username}</p>
            <p>Email : {displaydata.email.toLowerCase()}</p>
            <p>
              Address : {displaydata.address.street} {displaydata.address.suite}{" "}
              {displaydata.address.city} {displaydata.address.zipcode}
            </p>
            <p>Phone : {displaydata.phone}</p>
            <p>Website : {displaydata.website}</p>
            <p>Company : {displaydata.company.name}</p>
          </div>
          <div className="modal-footer">
            <button
              type="button"
              className="btn btn-default"
              data-dismiss="modal"
            >
              Close
            </button>
          </div>
        </div>
      </div>
    </div>
  );
}

export default Modal;
