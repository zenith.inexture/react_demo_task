import React from "react";
import useFetch from "../Custom Hooks/useFetch";

function Data2() {
  //useFetch is the custom hooks it's fetch the data form api
  const data = useFetch("https://dummyjson.com/users");

  return (
    <div className="container">
      <div className="row">
        <div className="col-md-12">
          <h2 style={{ textAlign: "center", textDecoration: "underline" }}>
            Phase two Users
          </h2>
        </div>
      </div>
      <div className="row">
        {data &&
          data.users.map((i, index) => (
            <div
              className="col-md-12"
              key={index}
              style={{ textAlign: "center" }}
            >
              <div
                className="boxalbums databox"
                style={{ height: "auto", padding: "10px" }}
              >
                <span style={{ fontWeight: "bold" }}>{i.firstName}</span>{" "}
                <span style={{ fontWeight: "bold" }}>{i.lastName}</span>
                <br></br>
              </div>
            </div>
          ))}
      </div>
    </div>
  );
}

export default Data2;
