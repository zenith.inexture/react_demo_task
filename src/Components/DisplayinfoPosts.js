import React from "react";
import { useLocation } from "react-router-dom";

function DisplayInfo() {
  const location = useLocation();
  // console.log(location);
  return (
    <div className="container">
      <div className="row">
        <div className="col-md-12">
          <h2 style={{ textAlign: "center", textDecoration: "underline" }}>
            All Posts of {location.state.username}
          </h2>
        </div>
      </div>
      <div className="row">
        {location.state.ak.map((i, index) => (
          <div className="col-md-6" key={index}>
            <div className="postbox">
              <p style={{ fontWeight: "bold" }}>
                {index + 1} : {i.title}
              </p>
              <p className="bodytext">{i.body}</p>
              <br></br>
            </div>
          </div>
        ))}
      </div>
    </div>
  );
}
export default DisplayInfo;
