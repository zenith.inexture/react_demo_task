import React, { useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useNavigate } from "react-router-dom";
import Modal from "./Modal";
import { fetchalbums, fetchsposts, fetchusers } from "./Redux/User/userAction";
import { UserConsumer } from "./userContext";

function Home() {
  const navigate = useNavigate();
  const [search, setSearch] = useState("");
  const [sort, setSort] = useState(false);
  const originaldata = useSelector((state) => state.users);
  const usersposts = useSelector((state) => state.posts);
  const useralbums = useSelector((state) => state.albums);
  const dispatch = useDispatch();
  const [displaydata, setDisplayData] = useState({});
  const [sortsearch, setSortSearch] = useState([]);

  React.useEffect(() => {
    dispatch(fetchusers());
    dispatch(fetchsposts());
    dispatch(fetchalbums());
    // eslint-disable-next-line
  }, []);

  const handlepersonaldata = (i) => {
    setDisplayData(i);
  };
  const handleposts = (id, name) => {
    console.log(name);
    console.log("handleposts");
    const userpost = usersposts.filter((i) => i.userId === id);
    navigate("displayinfoposts", {
      state: { ak: userpost, username: name },
    }); //this state is params pass during the route called
  };

  const handlealbums = (id, name) => {
    console.log(name);
    console.log("handlealbums");
    const useralbum = useralbums.filter((i) => i.userId === id);
    navigate("displayinfoalbums", {
      state: { array: useralbum, name: name },
    });
  };
  const handlesort = () => {
    if (sort) {
      originaldata.sort(function (a, b) {
        a = a?.name?.toLowerCase();
        b = b?.name?.toLowerCase();
        return a == b ? 0 : a > b ? 1 : -1;
      });
    } else {
      originaldata.sort(function (a, b) {
        a = a?.name?.toLowerCase();
        b = b?.name?.toLowerCase();
        return a == b ? 0 : b > a ? 1 : -1;
      });
    }
    setSort(!sort);
  };

  const handlesearch = (e) => {
    setSearch(e.target.value);
    if (e.target.value.length >= 3) {
      setSortSearch(
        originaldata.filter((i) =>
          i.name.toLowerCase().includes(e.target.value.toLowerCase())
        )
      );
    }
  };
  return (
    <section className="home_section">
      {console.log("home component")}
      <div className="container">
        <div className="row">
          <div className="col-md-12">
            Search :{" "}
            <div className={search.length >= 3 ? "dropdown open" : "dropdown"}>
              <input
                type="text"
                id="dropdownMenu1"
                aria-haspopup="true"
                aria-expanded="true"
                value={search}
                onChange={(e) => handlesearch(e)}
                style={{ cursor: "auto" }}
                className="searchinput"
              ></input>
              <ul
                className="dropdown-menu dropmenu"
                aria-labelledby="dropdownMenu1"
              >
                {sortsearch.length !== 0 ? (
                  sortsearch.map((i) => (
                    <li key={i.name}>
                      <a
                        onClick={() =>
                          navigate("displayinfo", {
                            state: { array: i },
                          })
                        }
                      >
                        {i.name}
                      </a>
                    </li>
                  ))
                ) : (
                  <li>No Data Available</li>
                )}
              </ul>
            </div>
          </div>
        </div>
        <div className="row">
          <div className="col-md-12">
            {originaldata.length !== 0 && (
              <>
                <table className="table_css">
                  <thead className="table_head">
                    <tr>
                      <th style={{ width: "5%" }}>#</th>
                      <th
                        style={{ width: "15%", cursor: "pointer" }}
                        onClick={() => handlesort()}
                      >
                        Name{" "}
                        {sort ? (
                          <i className="fa fa-angle-up" aria-hidden="true"></i>
                        ) : (
                          <i
                            className="fa fa-angle-down"
                            aria-hidden="true"
                          ></i>
                        )}
                      </th>
                      <th style={{ width: "13%" }}>Username</th>
                      <th style={{ width: "18%" }}>Email</th>
                      <th
                        style={{ width: "8%", textAlign: "center" }}
                        colSpan="3"
                      >
                        Other Details
                      </th>
                    </tr>
                  </thead>
                  <tbody className="table_body">
                    {originaldata.map((i, index) => {
                      return (
                        <tr key={index} className="body_row">
                          <td>{index + 1}</td>
                          <td>{i.name}</td>
                          <td>{i.username}</td>
                          <td>{i.email.toLowerCase()}</td>
                          <td>
                            <button
                              className="all_btn"
                              data-toggle="modal"
                              data-target="#lgmodal"
                              onClick={() => handlepersonaldata(i)}
                            >
                              Personal
                            </button>
                          </td>
                          <td>
                            <button
                              className="all_btn"
                              onClick={() => handleposts(i.id, i.name)}
                            >
                              Posts
                            </button>
                          </td>
                          <td>
                            <button
                              className="all_btn"
                              onClick={() => handlealbums(i.id, i.name)}
                            >
                              Albums
                            </button>
                          </td>
                        </tr>
                      );
                    })}
                  </tbody>
                </table>
                {Object.keys(displaydata).length !== 0 && (
                  <Modal>{displaydata}</Modal>
                )}
              </>
            )}
          </div>
        </div>
        <div className="row">
          <div className="col-md-2 col-md-offset-10">
            <UserConsumer>{(text) => <p>~ {text}</p>}</UserConsumer>
          </div>
        </div>
      </div>
    </section>
  );
}

export default Home;
