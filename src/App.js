import "./Assets/CSS/App.css";
import { AuthProvider } from "./Utils/auth";
import Navbar from "./Components/Navbar";
import Router from "./Router";
import { UserProvider } from "./Components/userContext";

function App() {
  return (
    <>
      <AuthProvider>
        <UserProvider value="Zenith">
          <Navbar />
          <Router />
        </UserProvider>
      </AuthProvider>
    </>
  );
}

export default App;
