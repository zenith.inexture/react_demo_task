import * as Yup from "yup";

//assign initial values
const initialValues = {
  username: "",
  email: "",
  password: "",
  cpassword: "",
  phone: "",
  state: "select",
};

//validation with yup object
const validationSchema = Yup.object({
  username: Yup.string().required("Required"),
  email: Yup.string()
    .required("Required")
    .matches(/^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,}$/, "Invalid email format"),
  password: Yup.string()
    .min(8, "Password is too short - should be 8 chars minimum.")
    .matches(/^[a-z0-9]+$/i, "Password can only contain letters and numbers")
    .required("Required"),
  cpassword: Yup.string()
    .oneOf([Yup.ref("password")], "Confirm password must match")
    .required("Required"),
  phone: Yup.string()
    .matches(
      /^[0-9]{10}$/,
      "phone can only contain numbers and exact 10 numbers are allowed"
    )
    .required("Required"),
  state: Yup.string()
    // .required("Required"),
    .test("", "Required", (values) => {
      if (values === "select") {
        return false;
      } else {
        return true;
      }
    }),
});

export { initialValues, validationSchema };
