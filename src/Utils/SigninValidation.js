import * as Yup from "yup";

//assign initial values
const initialValues = {
  signin_username: "",
  signin_password: "",
};

//validation with yup object
const validationSchema = Yup.object({
  signin_username: Yup.string().required("Required"),

  signin_password: Yup.string().required("Required"),
});

export { initialValues, validationSchema };
