import React from "react";
import { Route, Routes } from "react-router-dom";
import Dashboard from "./ProtectedScreen/Dashboard";
import Home from "./Components/Home";
import { RequireAuth } from "./Utils/RequireAuth";
import Signin from "./Screen/Signin";
import Signup from "./Screen/Signup";
import Data1 from "./Components/Data1";
import Data2 from "./Components/Data2";
import Nomatch from "./Components/Nomatch";
const LazyDisplyInfoPosts = React.lazy(() =>
  import("./Components/DisplayinfoPosts")
);
const LazyDisplyInfoAlbums = React.lazy(() =>
  import("./Components/DisplayinfoAlbums")
);
const LazyDisplyInfo = React.lazy(() => import("./Components/Displayinfo"));

function Router() {
  return (
    <>
      <Routes>
        <Route
          path="/"
          element={
            <RequireAuth>
              <Dashboard />
            </RequireAuth>
          }
        >
          <Route path="data1" element={<Data1 />}></Route>
          <Route path="data2" element={<Data2 />}></Route>
        </Route>
        <Route
          path="home"
          element={
            <RequireAuth>
              <Home />
            </RequireAuth>
          }
        ></Route>
        <Route path="signin" element={<Signin />}></Route>
        <Route path="signup" element={<Signup />}></Route>
        <Route
          path="home/displayinfoposts"
          element={
            <React.Suspense fallback="Loading...">
              <LazyDisplyInfoPosts />
            </React.Suspense>
          }
        ></Route>
        <Route
          path="home/displayinfo"
          element={
            <React.Suspense fallback="Loading...">
              <LazyDisplyInfo />
            </React.Suspense>
          }
        ></Route>
        <Route
          path="home/displayinfoalbums"
          element={
            <React.Suspense fallback="Loading...">
              <LazyDisplyInfoAlbums />
            </React.Suspense>
          }
        ></Route>
        <Route path="*" element={<Nomatch />}></Route>
      </Routes>
    </>
  );
}

export default Router;
