import React, { useEffect, useMemo, useRef } from "react";
import { NavLink, Outlet } from "react-router-dom";
import { useAuth } from "../Utils/auth";

function Dashboard() {
  const [count, setCount] = React.useState(0);
  const auth = useAuth();
  const newref = useRef();
  //handle sign out function
  const handlesignout = () => {
    auth.signout();
  };

  //if we use useMemo in this function then it's rerender when it's necessary
  const numberoftimes = useMemo(() => {
    console.log(`Welcome ${auth.user} run`);
    return `Welcome ${auth.user}`;
  }, [auth]);

  const hello = () => {
    setCount((count) => count + 1);
    if (count === 9) {
      newref.current.style.color = "blue";
    }
  };

  return (
    <section className="dashboard_section">
      {console.log("dashboard component")}
      <div className="container">
        <div className="row">
          <div className="col-md-12">
            <button className="btn btn-success" onClick={() => hello()}>
              AddLike
            </button>
            <span style={{ marginLeft: "20px" }}>{count}</span>
            <h3 ref={newref}>{numberoftimes}</h3>
            <NavLink to="/">
              <button className="btn btn-success" onClick={handlesignout}>
                Sign Out
              </button>
            </NavLink>
            <br></br>
            <br></br>
            <NavLink to="data1" style={{ marginRight: "10px" }}>
              <button className="btn btn-success">
                Phase one product of {auth.user}
              </button>
            </NavLink>
            <NavLink to="data2">
              <button className="btn btn-success">
                Phase two users of {auth.user}
              </button>
            </NavLink>
            <Outlet />
          </div>
        </div>
      </div>
    </section>
  );
}

export default Dashboard;
