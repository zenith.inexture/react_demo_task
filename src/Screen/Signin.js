import { useFormik } from "formik";
import { useState } from "react";
import { NavLink, useNavigate } from "react-router-dom";
import { validationSchema } from "../Utils/SigninValidation";
import { useAuth } from "../Utils/auth";
import axios from "axios";

//assign initial values
const initialValues = {
  signin_username: "",
  signin_password: "",
};

function Signin() {
  //create a state for display the error message
  const [userpassworderror, setUserpassworderror] = useState();
  const auth = useAuth();
  const navigate = useNavigate();

  //handlesignin function for display error message
  const handlesignin = (values) => {
    // const retrievedObject = JSON.parse(atob(localStorage.getItem("values")));
    // //store error in error state
    // setUserpassworderror(
    //   values.signin_username === retrievedObject.username &&
    //     values.signin_password === retrievedObject.password
    //     ? (auth.signin(values.signin_username),
    //       navigate("/", { replace: true }))
    //     : "Invalid credentials. Please try again."
    // );
    console.log(values);
    axios
      .post("http://localhost:5000/signinuser/checksigninuser", values)
      .then(
        (res) => (
          alert(res.data.message),
          auth.signin(values.signin_username),
          navigate("/", { replace: true })
        )
      )
      .catch((err) => setUserpassworderror(err.response.data.message));
  };

  const formik = useFormik({
    initialValues,
    validationSchema,
    onSubmit: (values) => {
      handlesignin(values);
    },
  });

  return (
    <section className="singin_section">
      <div className="container">
        <div className="row">
          <div className="col-md-6 col-md-offset-3">
            <h1 className="text-center" style={{ color: "blue" }}>
              Sign in
            </h1>
            <form onSubmit={formik.handleSubmit}>
              <div className="form-group">
                {userpassworderror && (
                  <p className="signin_error">{userpassworderror}</p>
                )}
              </div>
              <div className="form-group">
                <label htmlFor="signin_username">Username :</label>
                <input
                  type="text"
                  className="form-control"
                  id="signin_username"
                  placeholder="Username"
                  name="signin_username"
                  onChange={formik.handleChange}
                  value={formik.values.signin_username}
                />
                {formik.errors.signin_username &&
                formik.touched.signin_username ? (
                  <p className="error_msg">{formik.errors.signin_username}</p>
                ) : null}
              </div>
              <div className="form-group">
                <label htmlFor="signin_password">Password :</label>
                <input
                  type="password"
                  className="form-control"
                  id="signin_password"
                  placeholder="Password"
                  name="signin_password"
                  onChange={formik.handleChange}
                  value={formik.values.signin_password}
                />
                {formik.errors.signin_password &&
                formik.touched.signin_password ? (
                  <p className="error_msg">{formik.errors.signin_password}</p>
                ) : null}
              </div>
              <div className="form-group">
                <button type="submit" className="btn btn-success btn-center">
                  Sign in
                </button>
              </div>
              <div className="form-group">
                <h5>
                  Don't have an account
                  <NavLink to="/signup" className="signup-btn">
                    Sign up
                  </NavLink>
                </h5>
              </div>
            </form>
          </div>
        </div>
      </div>
    </section>
  );
}

export default Signin;
