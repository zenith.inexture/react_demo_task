import { useFormik } from "formik";
import { useNavigate } from "react-router-dom";
import axios from "axios";

import { initialValues, validationSchema } from "../Utils/SignupValidation";

function Signup() {
  const navigate = useNavigate();
  const formik = useFormik({
    initialValues,
    validationSchema,
    //onsubmit function
    onSubmit: (values) => {
      console.log(values);
      addFormValue(values);
      // localStorage.setItem("values", btoa(JSON.stringify(values)));
      // navigate("/signin");
    },
  });

  const addFormValue = (formvalues) => {
    console.log(formvalues);
    axios
      .post("http://localhost:5000/signupuser/addsignupuser", formvalues)
      .then((res) => (alert(res.data.message), navigate("/signin")))
      .catch((err) => alert(err.response.data.errors));
  };
  return (
    <section className="singup_section">
      <div className="container">
        <div className="row">
          <div className="col-md-6 col-md-offset-3">
            <h1 className="text-center" style={{ color: "blue" }}>
              Sign up
            </h1>
            <form onSubmit={formik.handleSubmit}>
              <div className="form-group">
                <label htmlFor="username">Username :</label>
                <input
                  type="text"
                  className="form-control"
                  id="username"
                  placeholder="Username"
                  name="username"
                  onChange={formik.handleChange}
                  onBlur={formik.handleBlur}
                  value={formik.values.uname}
                />
                {formik.errors.username && formik.touched.username ? (
                  <p className="error_msg">{formik.errors.username}</p>
                ) : null}
              </div>
              <div className="form-group">
                <label htmlFor="email">Email :</label>
                <input
                  type="email"
                  className="form-control"
                  id="email"
                  placeholder="Email"
                  name="email"
                  onChange={formik.handleChange}
                  onBlur={formik.handleBlur}
                  value={formik.values.email}
                />
                {formik.errors.email && formik.touched.email ? (
                  <p className="error_msg">{formik.errors.email}</p>
                ) : null}
              </div>
              <div className="form-group">
                <label htmlFor="password">Password :</label>
                <input
                  type="password"
                  className="form-control"
                  id="password"
                  placeholder="Password"
                  name="password"
                  onChange={formik.handleChange}
                  onBlur={formik.handleBlur}
                  value={formik.values.password}
                />
                {formik.errors.password && formik.touched.password ? (
                  <p className="error_msg">{formik.errors.password}</p>
                ) : null}
              </div>
              <div className="form-group">
                <label htmlFor="cpassword">Confirm password :</label>
                <input
                  type="password"
                  className="form-control"
                  id="cpassword"
                  placeholder="Confirm Password"
                  name="cpassword"
                  onChange={formik.handleChange}
                  onBlur={formik.handleBlur}
                  value={formik.values.cpassword}
                />
                {formik.errors.cpassword && formik.touched.cpassword ? (
                  <p className="error_msg">{formik.errors.cpassword}</p>
                ) : null}
              </div>
              <div className="form-group">
                <label htmlFor="phone">Phone :</label>
                <input
                  type="text"
                  className="form-control"
                  id="phone"
                  placeholder="Phone number"
                  name="phone"
                  onChange={formik.handleChange}
                  onBlur={formik.handleBlur}
                  value={formik.values.phone}
                />
                {formik.errors.phone && formik.touched.phone ? (
                  <p className="error_msg">{formik.errors.phone}</p>
                ) : null}
              </div>
              <div className="form-group">
                <label htmlFor="state">State :</label>
                <select
                  className="form-control"
                  id="state"
                  name="state"
                  onChange={formik.handleChange}
                  onBlur={formik.handleBlur}
                  value={formik.values.state}
                >
                  <option value="select">Select</option>
                  <option value="gujarat">Gujarat</option>
                  <option value="maharashtra">Maharashtra</option>
                  <option value="madhya pradesh">Madhya Pradesh</option>
                  <option value="delhi">Delhi</option>
                  <option value="kolkata">Kolkata</option>
                </select>
                {formik.errors.state && formik.touched.state ? (
                  <p className="error_msg">{formik.errors.state}</p>
                ) : null}
              </div>
              <div className="form-group">
                <button type="submit" className="btn btn-success btn-center">
                  Sign up
                </button>
              </div>
            </form>
          </div>
        </div>
      </div>
    </section>
  );
}

export default Signup;
